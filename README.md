# cs371g-netflix
Name: Michael Lee, Ewin Zuo

Gitlab: michaellee4, eztso

Git SHA: 1113cfa720c15092846b67a564689f1c1546eb59

Gitlab Pipelines: https://gitlab.com/michaellee4/cs371g-netflix/-/pipelines

Estimated Completion Time: 10 hours

Actual Completion Time: 12 hours

Comments:
- Cache generated locally from docker
- We used 3 different types of caches
  1. Average score a user gave a movies they watched
  2. Average score a movie received across all reviews
  3. Probe answers, for calculating RMSE
- Call 'make run' to see our predictor in action
- Sample probe run in probe.txt
- Our expected final RMSE should be 0.97
