.DEFAULT_GOAL := all
MAKEFLAGS     += --no-builtin-rules
SHELL         := bash

ASTYLE        := astyle
CHECKTESTDATA := checktestdata
CPPCHECK      := cppcheck
DOXYGEN       := doxygen
VALGRIND      := valgrind

ifeq ($(shell uname -s), Darwin)
    BOOST    := /usr/local/include/boost
    CXX      := g++
    CXXFLAGS := --coverage -pedantic -std=c++14 -O3 -I/usr/local/include -Wall -Wextra -lboost_serialization
    GCOV     := gcov
    LDFLAGS  := -lgtest -lgtest_main
else
    BOOST    := /usr/include/boost
    CXX      := g++
    CXXFLAGS := --coverage -pedantic -std=c++14 -O3 -Wall -Wextra -lboost_serialization
    GCOV     := gcov
    LDFLAGS  := -lgtest -lgtest_main -pthread
endif

# run docker
docker:
	docker run -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

# get git config
config:
	git config -l

# get git log
Netflix.log:
	git log > Netflix.log

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the Netflix code repo
pull:
	make clean
	@echo
	git pull
	git status

# upload files to the Netflix code repo
push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Netflix.cpp
	git add Netflix.hpp
	-git add Netflix.log
	-git add html
	git add makefile
	git add README.md
	git add RunNetflix.cpp
	git add RunNetflix.ctd
	git add TestNetflix.cpp
	git commit -m "another commit"
	git push
	git status

# remove executables and temporary files
clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunNetflix
	rm -f TestNetflix
	rm -f cache

# remove executables, temporary files, and generated files
scrub:
	make clean
	rm -f  Netflix.log
	rm -f  Doxyfile
	rm -rf netflix-tests
	rm -rf html
	rm -rf latex

# compile run harness
RunNetflix: Netflix.hpp Netflix.cpp RunNetflix.cpp
	-$(CPPCHECK) Netflix.cpp
	-$(CPPCHECK) RunNetflix.cpp
	$(CXX) $(CXXFLAGS) Netflix.cpp RunNetflix.cpp -o RunNetflix

# compile test harness
TestNetflix: Netflix.hpp Netflix.cpp TestNetflix.cpp
	-$(CPPCHECK) Netflix.cpp
	-$(CPPCHECK) TestNetflix.cpp
	$(CXX) $(CXXFLAGS) Netflix.cpp TestNetflix.cpp -o TestNetflix $(LDFLAGS)

# run/test files, compile with make all
FILES :=                                  \
    RunNetflix                            \
    TestNetflix

# compile all
all: $(FILES)

# check integrity of input file
ctd-check:
	$(CHECKTESTDATA) RunNetflix.ctd RunNetflix.in

# generate a random input file
ctd-generate:
	$(CHECKTESTDATA) -g RunNetflix.ctd RunNetflix.tmp

# execute run harness and diff with expected output
run: RunNetflix
	./RunNetflix < probe.txt

# execute test harness
test: TestNetflix
#	$(VALGRIND) ./TestNetflix
	./TestNetflix
	$(GCOV) Netflix.cpp | grep -B 2 "cpp.gcov"
cache:
	g++ -o cache generate_caches.cc -O3 -std=c++14 -lboost_serialization
	./cache

# test files in the Netflix test repo
TFILES := `ls netflix-tests/*.in`

# clone the Netflix test repo
netflix-tests:
	git clone https://gitlab.com/gpdowning/cs371g-netflix-tests.git netflix-tests

# execute run harness against a test in Netflix test repo and diff with expected output
netflix-tests/%: RunNetflix
	./RunNetflix < $@.in > RunNetflix.tmp
	diff RunNetflix.tmp $@.out

# execute run harness against all tests in Netflix test repo and diff with expected output
tests: netflix-tests RunNetflix
	-for  v in $(TFILES); do make $${v/.in/}; done

# auto format the code
format:
	$(ASTYLE) Netflix.cpp
	$(ASTYLE) Netflix.hpp
	$(ASTYLE) RunNetflix.cpp
	$(ASTYLE) TestNetflix.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile Netflix.hpp
	$(DOXYGEN) Doxyfile

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml                        \
    Netflix.log                           \
    html								  \
	ctd-check 
	
# uncomment the following line once you've pushed your test files
# you must replace GitLabID with your GitLabID
#    collatz-tests/GitLabID-RunNetflix

# check the existence of check files
check: $(CFILES)

# output versions of all tools
versions:
	@echo "% shell uname -p"
	@echo  $(shell uname -p)
	@echo
	@echo "% shell uname -s"
	@echo  $(shell uname -s)
	@echo
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version
	@echo
	@echo "% grep \"#define BOOST_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_VERSION " $(BOOST)/version.hpp
	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version
	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version
	@echo
	@$(CXX) --version
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version
	@echo
	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version
ifneq ($(shell uname -s), Darwin)
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif
	
