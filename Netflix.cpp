#include "Netflix.hpp"
#include <cmath>
#include <string>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <fstream>
#include <vector>
#include <unordered_map>
#include "boost/archive/binary_iarchive.hpp"
#include "boost/archive/binary_oarchive.hpp"
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/unordered_map.hpp>
#include <boost/serialization/unordered_set.hpp>

using namespace std;

// ------------
// NetflixPredictor
// ------------

// Loads in the pre-generated boost-serialized binaries for the caches we'll be using
// Since these caches can be large, we use RAII to manage their lifetimes.
NetflixPredictor::NetflixPredictor(bool load_caches) {
    if (!load_caches) return;
    {
        std::ifstream ifs("UserAverageScores.bin");
        boost::archive::binary_iarchive ia(ifs);
        ia >> user_avg_map;
    }
    {
        std::ifstream ifs("MovieAverageScores.bin");
        boost::archive::binary_iarchive ia(ifs);
        ia >> movie_avg_map;
    }
    {
        std::ifstream ifs("ProbeAnswers.bin");
        boost::archive::binary_iarchive ia(ifs);
        ia >> probe_answers;
    }
}

// Calculates a prediction using the method described here: https://www.science20.com/random_walk/predicting_movie_ratings_math_won_netflix_prize
// Essentially calculates an offset from the overall mean taking into account a given user's average ratings and a given movie's average ratings
double NetflixPredictor::PredictionHelper(int user_id, int movie_id) const {
    assert(user_id > 0);
    assert(movie_id > 0);
    // Cache sanity checks
    assert(user_avg_map.find(user_id) != user_avg_map.end());
    assert(movie_avg_map.find(movie_id) != movie_avg_map.end());
    assert(user_avg_map.at(user_id) > 0 && user_avg_map.at(user_id) <= 5);
    assert(movie_avg_map.at(movie_id) > 0 && movie_avg_map.at(movie_id) <= 5);

    double user_off = user_avg_map.at(user_id) - kOverallAverage;
    double movie_off = movie_avg_map.at(movie_id) - kOverallAverage;
    double res = kOverallAverage + user_off + movie_off;
    return std::floor((res) * 10) / 10;
}

// Uses the helper function above, but now we make sure to clamp the the return only to the valid range
// Also persist the prediction we made and the actual answer so we can calculate the RMSE later
double NetflixPredictor::MakePrediction(int user_id, int movie_id) {
    assert(user_id > 0);
    assert(movie_id > 0);

    double prediction = PredictionHelper(user_id, movie_id);
    prediction = std::min(std::max(prediction, 1.0),5.0);

    double actual = probe_answers[user_id][movie_id];
    results.push_back(actual);
    predictions.push_back(prediction);
    return prediction;
}

// Calculates RMSE using the formula given here: https://en.wikipedia.org/wiki/Root-mean-square_deviation
double NetflixPredictor::CalculateRMSE() const {
    assert(!results.empty());
    assert(!predictions.empty());

    double sum = 0;
    int N = results.size();
    for(int i = 0; i < N; i++) {
        sum += std::pow(results[i] - predictions[i], 2);
    }
    sum /= N;
    double res = std::pow(sum, 0.5);

    assert(res >= 0);
    return res;
}

// Harness for reading input and making predictions based on the following format
// 1234: <- 1  movie_id
// 234   <- 1+ user_id's
// 345   <-
// ... Repeat above sequence
void netflix_solve() {
    // Create the predictor, this also loads in the caches
    NetflixPredictor predictor(true);

    // Temporary buffer to use with std::getline
    std::string buffer;
    // stoi will find the first integer occurence in a string, stopping when it finds an invalid character
    // e.g stoi("1234:") -> 1234
    int movie_id = -1;
    while(std::getline(std::cin, buffer)) {
        // If this is a movie entry update the current movie we are querying for.
        // Otherwise make a prediction for the user and movie
        if(buffer.back() == ':') {
            movie_id = std::stoi(buffer);
            assert(movie_id > 0);
            std::cout << buffer << "\n";
        }
        else {
            int user_id = std::stoi(buffer);
            // MakePredictions will perform the necessary assertions
            double res = predictor.MakePrediction(user_id, movie_id);
            std::cout << std::fixed << std::setprecision(1) << res << "\n";
        }
    }

    double rmse = predictor.CalculateRMSE();
    std::cout << std::fixed << std::setprecision(2) << "RMSE: " << rmse << std::endl;

    // Not necessary in general. But for us to get full credit for this assignment this should pass
    assert(rmse >= 0);
    assert(rmse <= 1);
}
