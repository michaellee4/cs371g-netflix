#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>
#include "boost/archive/binary_iarchive.hpp"
#include "boost/archive/binary_oarchive.hpp"
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/unordered_map.hpp>
#include <boost/serialization/unordered_set.hpp>


void tokenize(std::string const &str, const char delim, std::vector<std::string> &out)
{
    out.clear();
	size_t start;
	size_t end = 0;

	while ((start = str.find_first_not_of(delim, end)) != std::string::npos)
	{
		end = str.find(delim, start);
		out.push_back(str.substr(start, end - start));
	}
}

int main() {
    const std::string kTrainingDataPath = "/usr/gcc/training/";
    const int kLeadingZeros = 7;
    std::string buf;
    int tmp;
    std::vector<std::string> tokens;
    std::unordered_map<int, std::unordered_map<int, int>> user_movie_scores;
    std::unordered_map<int, std::pair<int, int>> movie_average_score;
    std::unordered_map<int, std::pair<int, int>> user_average_score;
    for (int i = 1; i <= 17770; ++i) {
        const std::string n_str = std::to_string(i);
        const std::string filename = std::string("mv_") + std::string(kLeadingZeros - n_str.length(), '0') + n_str + std::string(".txt");
        const std::string path = kTrainingDataPath + filename;
        std::cout << path << std::endl;
        std::ifstream movie_file(path);
        movie_file >> tmp;
        std::getline(movie_file, buf);
        while (std::getline(movie_file, buf)) {
            tokenize(buf, ',', tokens);
            const int user = std::stoi(tokens[0]);
            const int score = std::stoi(tokens[1]);
            user_movie_scores[user][i] = score;

            movie_average_score[i].first += score;
            movie_average_score[i].second++;

            user_average_score[user].first += score;
            user_average_score[user].second++;
        }

    }

    const std::string kProbePath = "probe.txt";

    std::unordered_map<int, std::unordered_map<int, int>> probe_answers;
    std::unordered_map<int, double> user_avgs, movie_avgs;

    std::ifstream movie_file(kProbePath);
    std::getline(movie_file, buf);
    int cur_movie = stoi(buf);
    while (std::getline(movie_file, buf)) {
        int num = stoi(buf);
        if (buf.back() == ':') {
            cur_movie = num;
        } else {
            probe_answers[num][cur_movie] = user_movie_scores[num][cur_movie];
        }
    }

    for (const auto& user_p : user_average_score) {
        const int user = user_p.first;
        const auto intpair = user_p.second;
        user_avgs[user] = (float) intpair.first / (float) intpair.second;
    }
    for (const auto& movie_p : movie_average_score) {
        const int movie = movie_p.first;
        const auto intpair = movie_p.second;
        movie_avgs[movie] = (float) intpair.first / (float) intpair.second;
    }

    using namespace boost::archive;
    std::cout << "Serializing Probe Answers" << std::endl;
    {
        std::ofstream ofs("ProbeAnswers.bin");
        binary_oarchive boa(ofs);
        boa << probe_answers;
    }

    std::cout << "Serializing User Average Scores" << std::endl;
    {
        std::ofstream ofs("UserAverageScores.bin");
        binary_oarchive boa(ofs);
        boa << user_avgs;
    }

    std::cout << "Serializing Probe Answers" << std::endl;
    {
        std::ofstream ofs("MovieAverageScores.bin");
        binary_oarchive boa(ofs);
        boa << movie_avgs;
    }
}
