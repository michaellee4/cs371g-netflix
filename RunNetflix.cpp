// --------------
// RunNetflix.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Netflix.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    // We're reading and printing 1.4 million lines. Might as well make it faster.
    ios_base::sync_with_stdio(false);
    netflix_solve();
    return 0;
}
