#include <iostream>
#include <sstream>
#include <vector>
#include <unordered_map>


#pragma once

// ------------
// Netflix Predictor
// ------------

//! A class that manages the lifecycle of caches used for Netflix rating predictions
/**
 * Implemented using a segment tree under the hood. This trades off O(N)
 * preprocessing to eagerly cache collatz cycle lengths for all numbers in the
 * allowed input range in order to achieve O(logN) range maximum queries performance.
 */
class NetflixPredictor {
private:
    /**
     * Stores the average score a specific user has given to all movies they've rated
     */
    std::unordered_map <int, double> user_avg_map;
    /**
     * Stores the average score of all ratings this movie has received
     */
    std::unordered_map <int, double> movie_avg_map;
    /**
     * Used solely as a reference, this stores the actual score a user has given to a movie
     */
    std::unordered_map <int, std::unordered_map<int, int>> probe_answers;
    /**
     * A vector containing the actual score pertaining to the i'th query
     */
    std::vector<double> results;
    /**
     * A vector containing the prediction we made for the i'th query
     */
    std::vector<double> predictions;
    /**
     * Represents the average score of all ratings across all movies in the training data.
     */
    const double kOverallAverage = 3.60429;
    /**
     * A Pure function for calculating a prediction to make, given a user id and movie id
     * @param user_id integer that uniquely represents a given user
     * @param movie_id integer that uniquely represents a given movie
     * @return an double in the range of [1.0, 5.0] representing what score we think this user would give to this movie
     */
    double PredictionHelper(int user_id, int movie_id) const;
public:
    /**
     * Class constructor
     * @param load_caches loads boost-serialized caches into memory if set to true.
     */
    NetflixPredictor(bool load_caches);

    /**
     * A function for making a prediction to make, given a user id and movie id
     * Note: This function mutates the class by adding to the results and predictions vectors
     * @param user_id integer that uniquely represents a given user
     * @param movie_id integer that uniquely represents a given movie
     * @return an double in the range of [1.0, 5.0] representing what score we think this user would give to this movie
     */
    double MakePrediction(int user_id, int movie_id);
    /**
     * A function for calculating the current RMSE based on past predictions and answers
     * More details: https://en.wikipedia.org/wiki/Root-mean-square_deviation
     */
    double CalculateRMSE() const;

    /**
     * Friend class used solely for unit testing private member functions
     */
    friend class TEST_GTestHelper;
};

/**
 * A function for running a single test case in the format of probe.txt
 */
void netflix_solve();