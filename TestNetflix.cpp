// ---------------
// TestNetflix.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <iterator> // istream_iterator
#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair
#include <limits>
#include <cmath>

#include "gtest/gtest.h"

#include "Netflix.hpp"

using namespace std;

// This class is declared as a friend of NetflixPredictor to modify it's internal state for testing
class TEST_GTestHelper {
public:
    double PredictionHelperFriend(const NetflixPredictor& predictor, int user_id, int movie_id) {
        return predictor.PredictionHelper(user_id, movie_id);
    }
    void InsertPrediction(NetflixPredictor& predictor, double prediction, double actual) {
        predictor.predictions.push_back(prediction);
        predictor.results.push_back(actual);
    }
    void MockCaches(NetflixPredictor& predictor, int user_id, double user_rating_avg, int movie_id, double movie_rating_avg) {
        predictor.user_avg_map[user_id] = user_rating_avg;
        predictor.movie_avg_map[movie_id] = movie_rating_avg;
    }
    const std::vector<double>& predictorResults(const NetflixPredictor& predictor) {
        return predictor.results;
    }
    const std::vector<double>& predictorPredictions(const NetflixPredictor& predictor) {
        return predictor.predictions;
    }
};

// -----------
// TestNetflix
// -----------
bool double_equality(double a, double b) {
    return std::abs(a - b) < 0.000001;
}

TEST(NetflixFixture, BasicRMSETest1) {
    NetflixPredictor p(false);
    TEST_GTestHelper g;
    g.InsertPrediction(p, 3.0, 3.0);
    assert(double_equality(p.CalculateRMSE(), 0));
}

TEST(NetflixFixture, BasicRMSETest2) {
    NetflixPredictor p(false);
    TEST_GTestHelper g;
    g.InsertPrediction(p, 3.0, 4.0);
    assert(double_equality(p.CalculateRMSE(), 1));
}

TEST(NetflixFixture, BasicRMSETest3) {
    NetflixPredictor p(false);
    TEST_GTestHelper g;
    g.InsertPrediction(p, 3.0, 4.0);
    g.InsertPrediction(p, 2.0, 5.0);

    assert(double_equality(p.CalculateRMSE(), std::sqrt(5)));
}

TEST(NetflixFixture, BasicRMSETest4) {
    NetflixPredictor p(false);
    TEST_GTestHelper g;
    g.InsertPrediction(p, 2.0, 4.0);
    g.InsertPrediction(p, 2.0, 5.0);
    g.InsertPrediction(p, 4.0, 2.0);
    g.InsertPrediction(p, 1.0, 10.0);
    g.InsertPrediction(p, 5.0, 4.0);

    assert(double_equality(p.CalculateRMSE(), std::sqrt((float)99/(float)5)));
}

TEST(NetflixFixture, BasicRMSETest5) {
    NetflixPredictor p(false);
    TEST_GTestHelper g;
    g.InsertPrediction(p, 1.0, 1.0);
    g.InsertPrediction(p, 2.0, 2.0);
    g.InsertPrediction(p, 3.0, 3.0);
    g.InsertPrediction(p, 4.0, 4.0);
    g.InsertPrediction(p, 5.0, 5.0);

    assert(double_equality(p.CalculateRMSE(), 0));
}

TEST(NetflixFixture, BasicRMSETest6) {
    NetflixPredictor p(false);
    TEST_GTestHelper g;
    g.InsertPrediction(p, 5.0, 1.0);
    g.InsertPrediction(p, 4.0, 2.0);
    g.InsertPrediction(p, 3.0, 3.0);
    g.InsertPrediction(p, 2.0, 4.0);
    g.InsertPrediction(p, 1.0, 5.0);

    assert(double_equality(p.CalculateRMSE(), std::sqrt(8)));
}

TEST(NetflixFixture, TestPredictionHelper1) {
    NetflixPredictor p(false);
    TEST_GTestHelper g;
    g.MockCaches(p, 1, 2, 3, 4);
    const double pred = g.PredictionHelperFriend(p, 1, 3);
    // Not going to test actual numbers since that's a necessary abstraction, but there are certain guarantees we can
    // make about the prediction
    assert(pred >= 0);
    assert(pred <= 5);
}

TEST(NetflixFixture, TestPredictionHelper2) {
    NetflixPredictor p(false);
    TEST_GTestHelper g;
    g.MockCaches(p, 99, 3, 100, 2);
    g.MockCaches(p, 1, 3, 88, 2);
    g.MockCaches(p, 2, 3, 2, 2);
    g.MockCaches(p, 5, 3, 1, 2);
    g.MockCaches(p, 77, 3, 5, 2);

    const double pred = g.PredictionHelperFriend(p, 99, 5);

    assert(pred >= 0);
    assert(pred <= 5);
}

TEST(NetflixFixture, MakePrediction1) {
    NetflixPredictor p(false);
    TEST_GTestHelper g;
    g.MockCaches(p, 1, 2, 3, 4);
    p.MakePrediction(1, 3);


    assert(g.predictorResults(p).size() == 1);
    assert(g.predictorPredictions(p).size() == 1);
}

TEST(NetflixFixture, MakePrediction2) {
    NetflixPredictor p(false);
    TEST_GTestHelper g;
    g.MockCaches(p, 1, 2, 3, 4);
    g.MockCaches(p, 2, 2, 4, 4);
    g.MockCaches(p, 5, 2, 2, 4);
    g.MockCaches(p, 99, 2, 1000, 4);

    p.MakePrediction(1, 3);
    p.MakePrediction(2, 4);
    p.MakePrediction(5, 2);
    p.MakePrediction(99, 1000);

    assert(g.predictorResults(p).size() == 4);
    assert(g.predictorPredictions(p).size() == 4);
}

TEST(NetflixFixture, MakePrediction3) {
    NetflixPredictor p(false);
    TEST_GTestHelper g;

    for (int i = 1; i < 1001; ++i) {
        g.MockCaches(p, i, 1, i + 2, 2);
        p.MakePrediction(i, i + 2);
    }

    assert(g.predictorResults(p).size() == 1000);
    assert(g.predictorPredictions(p).size() == 1000);

    assert(double_equality(p.CalculateRMSE(), 1.0));
}

TEST(NetflixFixture, MakePrediction4) {
    NetflixPredictor p(false);
    TEST_GTestHelper g;

    for (int i = 999; i < 3333; ++i) {
        g.MockCaches(p, i, 1, i + 2, 2);
        p.MakePrediction(i, i + 2);
    }

    assert(g.predictorResults(p).size() == 3333 - 999);
    assert(g.predictorPredictions(p).size() == 3333 - 999);

    assert(double_equality(p.CalculateRMSE(), 1.0));
}
